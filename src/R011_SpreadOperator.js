import React, { Component } from 'react';

class R011_SpreadOperator extends Component {

    constructor(props) {
        super(props);
        this.state = {};        
    }

    componentDidMount() {
        var varArray1 = ['num1', 'num2'];
        var varArray2 = ['num3', 'num4'];
        var sumVarArr = [varArray1[0], varArray1[1], varArray2[0], varArray2[1]];
        console.log('1. sumVarArr : ' + sumVarArr);

        let sumLetArr = [...varArray1, ...varArray2];
        console.log('2. sumLetArr : ' + sumLetArr);
        const [sum1, sum2, ...abc] = sumLetArr;
        console.log('3. sum1 : ' +sum1+ ' , sum2 : ' +sum2+ ' , remain : ' + abc);

        var varObj1 = {key1 : 'val1', key2 : 'val2'};
        var varObj2 = {key2 : 'new2', key3 : 'val3'};
        var sumVarObj = Object.assign({}, varObj1, varObj2);
        console.log('4. sumVarObj : ' + JSON.stringify(sumVarObj));
        let sumLetObj = {...varObj1, ...varObj2};
        console.log('5. sumLetObj : ' + JSON.stringify(sumLetObj));

        var {key2, keyB, ...ddd} = sumLetObj;
        console.log('6. key2 : ' + key2 + ', keyB : ' + keyB + ', others : ' + JSON.stringify(ddd));

        
    }



    render() {
        return (
            <h2>[this is R011_SpreadOperator]</h2>
        )
    }
    
}

export default R011_SpreadOperator;