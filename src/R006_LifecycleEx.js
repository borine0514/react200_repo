import React, { Component } from 'react';

class R006_LifecycleEx extends Component {
    // getDerivedStateFromProps 는 constructor 다음으로 수행됨
    static getDerivedStateFromProps(props, state) {
        console.log('2. getDerivedStateFromProps Call : ' + props.prop_value);
        return {};
    }

    constructor(props) {
        super(props);
        this.state = {};
        console.log('1. constructor Call : ' + props.prop_value);
    }

    render() {
        console.log('3. render Call');
        return (
            <h2>[this is constructor function]</h2>
        )
    }
    
}

export default R006_LifecycleEx;